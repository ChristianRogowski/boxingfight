package sda.fight;

public interface IFighter {
    boolean isAlive();

    FighterAttackActionType attack();

    FighterDefenceActionType defend();

    void decreaseHp(int power);

    String getName();

    int getHp();

    int getPower();
}
