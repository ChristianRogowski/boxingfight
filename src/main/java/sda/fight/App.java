package sda.fight;

public class App
{
    public static void main( String[] args ) {

        IFighter fighter1 = new AggressiveBoxer("Tyson", 70, IFighterStyle.HIGH, 7);
        IFighter fighter2 = new Boxer("Gołota", 70, IFighterStyle.LOW, 4);
        IFightingMatch match = new BoxingMatch(fighter1, fighter2);
        match.fight();
    }
}
